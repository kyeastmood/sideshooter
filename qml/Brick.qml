import QtQuick 2.0
import Felgo 3.0

// Basic building block of a level, all moving entities collide with it.
EntityBaseDraggable {
    id: root
    entityType: "brick"
    width: 40
    height: 40
    // Setting colliderComponent is required for dragging in LevelEditor, however during
    // gameplay it must be switched off because the entity will collide with everything.
    colliderComponent: gameLogic && gameLogic.isInEditor ? collider : undefined
    selectionMouseArea.anchors.fill: root
    // Snaps to grid when editing level
    gridSize: 40

    onEntityPressAndHold: removeEntity()

    BoxCollider {
        id: collider
        bodyType: Body.Static
        categories: Box.Category4 // 4 = bricks only
        anchors.centerIn: parent
    }

    SpriteSequence {
        id: sprite
        anchors.centerIn: parent
        width: root.width
        height: root.height
        Sprite {
            frameCount: 1
            frameRate: 10
            frameWidth: root.width * 2
            frameHeight: root.height * 2

            source: Qt.resolvedUrl("../assets/brick.png")
        }
    }

}
