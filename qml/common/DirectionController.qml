import QtQuick 2.0
import Felgo 3.0

/* This component computes velocity factor and direction for an observer entity,
   based on target entity location. The observer entity can use resulting
   xAxisFactor and yAxisFactor to modify its motion and reach destination on
   both axes simultaneously. Destination can be a static point or EntityBase.

   This solution allows to move at maximum allowed velocity diagonally while
   not exceeding maximum velocity on X an Y axes. (If entity moved at maximum
   velocity on both axes at the same time then its diagonal velocity would be
   bigger than maximum allowed velocity.)

   How it works:

   First, a diagonal distance between source and destination is computed
   from horizontal and vertical distance using Pythagorean theorem.

   Because all three distances have to be covered at the same time and we want
   to move diagonally at maximum speed, we can scale velocities on X and Y axes
   by using simple proportion.
  */
Item {
    id: root

    /* This is the computed factor to scale and change direction of velocity
       of observer entity on X axis. E.g. if the factor is -0.47 it means the
       entity should move left at 0.47 * xMaxVelocity
      */
    readonly property alias xAxisFactor: priv.xAxisFactorPriv

    /* This is the computed factor to scale and change direction of velocity
       of observer entity on Y axis. E.g. if the factor is -0.47 it means the
       entity should move up at 0.47 * xMaxVelocity
      */
    readonly property alias yAxisFactor: priv.yAxisFactorPriv

    // Entity to follow; it takes precedence over targetPoint
    property EntityBase targetEntity

    // Target point to reach; not used if targetEntity is set
    property point targetPoint: Qt.point(-1,-1)

    // The entity for which we set velocity factors
    property EntityBase observerEntity

    /* Distance lower than this value will not be considered and the axis factor
       will be 0. */
    property int lowDistanceTreshold: 5

    // Update the factors immediately after assigning new target point or entity.
    onTargetEntityChanged: priv.update()
    onTargetPointChanged: priv.update()

    QtObject {
        id: priv

        /* We must keep separate properties for both observer and target entity
           coordinates to track their changes and update the factors. It is also
           needed to detect if the destination has been reached. */
        property real xObserverEntity: observerEntity.x
        property real yObserverEntity: observerEntity.y
        property real xTargetEntity: targetEntity ? targetEntity.x : targetPoint.x
        property real yTargetEntity: targetEntity ? targetEntity.y : targetPoint.y

        onXObserverEntityChanged: update()
        onYObserverEntityChanged: update()
        onXTargetEntityChanged: update()
        onYTargetEntityChanged: update()

        // Private factors, the public ones are only aliases to these.
        property real xAxisFactorPriv: 0
        property real yAxisFactorPriv: 0

        function update() {
            // It there is nothing to follow or reach the factors should be 0.
            if ((targetPoint.x === -1 || targetPoint.y === -1) && !targetEntity) {
                xAxisFactorPriv = 0
                yAxisFactorPriv = 0
                return
            }

            var xTarget = 0
            var yTarget = 0

            if (targetPoint.x >= 0 && targetPoint.y >= 0) {
                xTarget = targetPoint.x
                yTarget = targetPoint.y
            }

            // Overwrites target point
            if (targetEntity) {
                xTarget = targetEntity.x
                yTarget = targetEntity.y
            }

            var xDistance = xTarget - observerEntity.x
            var yDistance = yTarget - observerEntity.y
            // Pythagorean theorem
            var diagonalDistance = Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2))

            // If distance it too low, set factor to 0; otherwise compute the factor using proportion
            xAxisFactorPriv = Math.abs(xDistance) < lowDistanceTreshold ? 0 : xDistance / diagonalDistance
            yAxisFactorPriv = Math.abs(yDistance) < lowDistanceTreshold ? 0 : yDistance / diagonalDistance
        }
    }
}
