import QtQuick 2.0
import Felgo 3.0

/* SmoothMotionController gradually increases/decreases velocity until it reaches provided
   target velocity, which results in a smooth motion. */
Item {
    id: root

    // Collider is provided from outside
    property ColliderBase collider

    // Target velocities to reach.
    property int xTargetVelocity: 0
    property int yTargetVelocity: 0

    // How much velocity should increase/decrease in 1000 miliseconds.
    // This is not a real acceleration.
    property int xAbsAcceleration: 0
    property int yAbsAcceleration: 0

    // What value should be considered "low" for velocity when decelerating,
    // so xAlmostStopped or yAlmostStopped turn true.
    property int absLowVelocityThreshold: 0

    // Whether deceleration lowered velocity below low velocity threshold.
    readonly property bool xAlmostStopped: absLowVelocityThreshold > 0
                                           && Math.abs(xTargetVelocity) <= absLowVelocityThreshold
                                           && Math.abs(collider.linearVelocity.x) <= absLowVelocityThreshold

    readonly property bool yAlmostStopped: absLowVelocityThreshold > 0
                                           && Math.abs(yTargetVelocity) <= absLowVelocityThreshold
                                           && Math.abs(collider.linearVelocity.y) <= absLowVelocityThreshold

    // Whether target velocity on a given axis was reached.
    readonly property bool xTargetVelocityReached: collider && collider.linearVelocity.x === xTargetVelocity
    readonly property bool yTargetVelocityReached: collider && collider.linearVelocity.y === yTargetVelocity

    /* When target velocity changes for a given axis, current velocity must
       start to be updated in direction of the new taret velocity. */
    onXTargetVelocityChanged: priv.updateX()
    onYTargetVelocityChanged: priv.updateY()

    Timer {
        id: timer
        repeat: true
        interval: 33 // 30 FPS
        running: !xTargetVelocityReached || !yTargetVelocityReached
        onTriggered: {
            priv.updateX()
            priv.updateY()
        }
    }

    QtObject {
        id: priv

        function updateX() {
            if (!collider)
                return

            // Update collider velocity to a new value
            collider.linearVelocity.x = priv.getNewVelocity(collider.linearVelocity.x,
                                                            xTargetVelocity,
                                                            xAbsAcceleration)
        }

        function updateY() {
            if (!collider)
                return
            // Update collider velocity to a new value
            collider.linearVelocity.y = priv.getNewVelocity(collider.linearVelocity.y,
                                                            yTargetVelocity,
                                                            yAbsAcceleration)
        }

        // Returns a new velocity based on current target velocity and "acceleration".
        function getNewVelocity(currentVelocity, targetVelocity, absAcceleration) {
            if (currentVelocity === targetVelocity)
                return currentVelocity

            // How much should we change the value in this update,
            // based on interval and acceleration
            var accelerationStepPerUpdate = absAcceleration * timer.interval / 1000
            var wasSmaller = currentVelocity < targetVelocity
            var accelerationDirection = targetVelocity > currentVelocity ? 1 : -1
            // Update velocity by "step" and in the right direction
            var newVelocity = currentVelocity + accelerationStepPerUpdate * accelerationDirection
            var isSmaller = newVelocity < targetVelocity

            /* This check is important to detect whether we passed target velocity threshold.
               For example if target velocity is 100, current velocity is 85 and acceleration
               step is 27, then before the change current velocity is smaller then target
               velocity. But when we add 85 + 27 it equals 112 which means that we increased
               current velocity too much and must reduce it to target velocity (100). */
            if (wasSmaller !== isSmaller)
                newVelocity = targetVelocity;

            return newVelocity
        }
    }
}
