import QtQuick 2.0
import QtQuick.Layouts 1.12

import Felgo 3.0

SceneBase {

    signal menuRequested()

    property bool victory: false
    property int score: 0

    // background
    Rectangle {
        anchors.fill: parent.gameWindowAnchorItem
        color: victory ?"#5cab5e" : "black"
    }

    Keys.onPressed: {
        if (event.key === Qt.Key_Escape) {
            menuRequested()
            event.accepted = true;
        }
    }

    ColumnLayout {
        anchors.centerIn: parent

        Text {
            font.pointSize: 32
            horizontalAlignment: Qt.AlignHCenter
            color: "white"
            text: victory ? "Victory!" : "You've lost!"
            Layout.fillWidth: true
        }

        Text {
            font.pointSize: 18
            horizontalAlignment: Qt.AlignHCenter
            color: "white"
            text: "Your score: " + score
            Layout.fillWidth: true
        }

        // spacer
        Item {
            height: 50
        }

        MenuButton {
            text: "Back to menu"
            onClicked: menuRequested()
        }
    }

}
