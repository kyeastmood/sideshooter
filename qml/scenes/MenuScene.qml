import QtQuick 2.0
import QtQuick.Layouts 1.12
import Felgo 3.0

SceneBase {

    signal startGamePressed()
    signal startEditorPressed()
    signal exitPressed()

    // background
    Rectangle {
        anchors.fill: parent.gameWindowAnchorItem
        color: "#50459b"
    }

    Keys.onPressed: {
        if (event.key === Qt.Key_Escape) {
            exitPressed()
            event.accepted = true;
        }
        else if (event.key === Qt.Key_E) {
            editorButton.visible = true
            event.accepted = true;
        }
    }

    ColumnLayout {
        anchors.centerIn: parent
        MenuButton {
            text: "Start game"
            onClicked: startGamePressed()
        }
        MenuButton {
            id : editorButton
            // By default hide level editor from users as it's very basic
            visible: false
            text: "Editor (pre-alpha)"
            onClicked: startEditorPressed()
        }
        MenuButton {
            text: "Exit"
            onClicked: exitPressed()
        }
    }

}
