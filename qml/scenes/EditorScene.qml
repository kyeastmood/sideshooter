import QtQuick 2.0
import Felgo 3.0

import "../player"
import "../enemy"

// Basic level editor to create a simple level, not to be shown to users (the editor, not the level).
GameScene {
    id: root
    state: "levelEditing"

    Row {
        id: buildEntityButtons
        visible: true
        anchors.top: root.top

        BuildEntityButton {
            width: 50;
            height: 20
            toCreateEntityType: "../Brick.qml"
            Text { text: "Brick"; color: "white"; anchors.fill: parent }
        }
        BuildEntityButton {
            width: 50;
            height: 20
            toCreateEntityType: "../player/Player.qml"
            Text { text: "Player"; color: "white"; anchors.fill: parent }
        }
        BuildEntityButton {
            width: 50;
            height: 20
            toCreateEntityType: "../enemy/EnemyAircraft.qml"
            Text { text: "Enemy"; color: "white"; anchors.fill: parent }
        }
        BuildEntityButton {
            width: 50;
            height: 20
            toCreateEntityType: "../enemy/EnemyStronger.qml"
            Text { text: "Enemy stronger"; color: "white"; anchors.fill: parent }
        }
    }

    Row {
        anchors.bottom: parent.bottom

        SimpleButton {
            text: "New"
            onClicked: levelEditor.createNewLevel()
        }

        SimpleButton {
            text: "Save"
            onClicked: {
                if (levelEditor.currentLevelNameString === "")
                    saveAsButton.saveAs()
                else
                    levelEditor.saveCurrentLevel()
            }
        }

        SimpleButton {
            id: saveAsButton
            text: "Save as..."
            onClicked: saveAs()

            function saveAs() {
                nativeUtils.displayTextInput("Enter levelName",
                                             "",
                                             levelEditor.currentLevelName)
            }

            Connections {
                target: nativeUtils
                onTextInputFinished: {
                    if(accepted) {
                        levelEditor.saveCurrentLevel( {levelMetaData: {levelName: enteredText}} )
                    }
                }
            }
        }

        SimpleButton {
            text: "Export"
            onClicked: {
                levelEditor.exportLevelAsFile()
            }

            Connections {
                target: nativeUtils
                onTextInputFinished: {
                    if(accepted) {
                        levelEditor.saveCurrentLevel( {levelMetaData: {levelName: enteredText}} )
                    }
                }
            }
        }

        SimpleButton {
            text: "Show All"
            onClicked: {
                levelEditor.loadAllLevelsFromStorageLocation(levelEditor.applicationJSONLevelsLocation)
                // levelEditor.loadAllLevelsFromStorageLocation(levelEditor.authorGeneratedLevelsLocation)
                levelSelectionList.visible = true
            }
        }
    }

    LevelSelectionList {
        id: levelSelectionList
        width: 150
        height: 100
        z: 3
        anchors.centerIn: parent
        levelMetaDataArray: levelEditor.applicationJSONLevels
        // levelMetaDataArray: levelEditor.authorGeneratedLevels
        visible: false
        onLevelSelected: {
            levelEditor.loadSingleLevel(levelData)
            levelSelectionList.visible = false
        }
    }
}
