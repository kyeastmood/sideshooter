import QtQuick 2.0
import Felgo 3.0
import QtQuick.Layouts 1.12

StyledButton {
    Layout.fillWidth: true
    color: "#adadad"
    textColor: "#ffffff"
    radius: 0
    borderWidth: 0
}
