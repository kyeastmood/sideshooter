import QtQuick 2.0
import Felgo 3.0

import "../player"
import "../enemy"

SceneBase {
    id: root

    // the "logical size" - the scene content is auto-scaled to match the GameWindow size
    width: 480
    height: 320

    // Used by EditorScene
    readonly property alias entityManager: entityManager
    readonly property alias levelEditor: levelEditor
    readonly property alias camera: camera
    readonly property alias entityContainer: entityContainer

    signal escapePressed()
    signal gameFinished(bool victory, int score)

    function startGame() {
        levelEditor.loadSingleLevel({"levelId":5369, "storageLocation":"applicationJSON"})
        physicsWorld.running = true
        keysTimer.start()
    }

    function closeGame() {
        entityManager.removeAllEntities()
        gameLogic.score = 0
        gameLogic.createdEnemyCount = 0
        gameLogic.destroyedEnemyCount = 0
        physicsWorld.running = false
    }

    // Contains information about GameScene
    Item  {
        id : gameLogic
        // When level is being loaded and player component has been created, it updates
        // this property so e.g. key presses can be passed to it.
        property EntityBase player
        readonly property bool isInEditor: root.state == "levelEditing"
        property int score: 0
        // Total number of enemy units that were created while loading the level
        property int createdEnemyCount: 0
        property int destroyedEnemyCount: 0

        function onEnemyDestroyed(entityType) {
            ++destroyedEnemyCount
            if (entityType === "enemyStronger")
                score += 2
            else
                ++score
            if (createdEnemyCount - destroyedEnemyCount == 0)
                root.gameFinished(true, gameLogic.score)
        }

        Connections {
            target: gameLogic.player

            onEnergyChanged: {
                if (gameLogic.player.energy <= 0)
                    root.gameFinished(false, gameLogic.score)
            }
        }
    }

    EntityManager {
        id: entityManager
        entityContainer: entityContainer

        // required for LevelEditor, so the entities can be created by entityType
        dynamicCreationEntityList: [
            Qt.resolvedUrl("../Brick.qml"),
            Qt.resolvedUrl("../player/Player.qml"),
            Qt.resolvedUrl("../enemy/EnemyAircraft.qml"),
            Qt.resolvedUrl("../enemy/EnemyStronger.qml")
        ]
    }

    // In GameScene LevelEditor is only used for loading levels
    LevelEditor {
        id: levelEditor
        entityManagerItem: entityManager
        toRemoveEntityTypes: [ "brick", "player", "enemyAircraft", "enemyStronger" ]
        toStoreEntityTypes: [ "brick", "player", "enemyAircraft", "enemyStronger" ]
        applicationJSONLevelsDirectory: "../../levels/"
    }

    Item {
        id: entityContainer

        PhysicsWorld {
            id: physicsWorld
            gravity.y: 0.0
            running: false
        }
    }

    Camera {
        id: camera

        // set the gameWindowSize and entityContainer with which the camera works with
        gameWindowSize: Qt.point(gameScene.gameWindowAnchorItem.width,
                                 gameScene.gameWindowAnchorItem.height)
        entityContainer: entityContainer
        focusedObject: root.state != "levelEditing" ? gameLogic.player : null
        mouseAreaEnabled: false
    }

    Keys.forwardTo: [gameLogic.player ? gameLogic.player.controller : null, root]

    Keys.onPressed: {
        if (event.key === Qt.Key_Escape) {
            escapePressed()
            event.accepted = true;
        }
        /* Toggle camera dragging for editor. This is a workaround for
           a problem that when mouseAreaEnabled equals true, dragging object
           in the level is not possible. */
        else if (event.key === Qt.Key_M && root.state == "levelEditing") {
            camera.mouseAreaEnabled  = !camera.mouseAreaEnabled
        }
    }

    // "HUD"
    Text {
        id: energyText
        text: gameLogic.player ? "Energy: " + gameLogic.player.energy : ""
        visible: root.state != "levelEditing"
        font.pointSize: 18
        color: "white"
    }
    Text {
        anchors.left: energyText.right
        anchors.leftMargin: 20
        text: "Score: " + gameLogic.score
        visible: root.state != "levelEditing"
        font.pointSize: 18
        color: "white"
    }

    Text {
        id: keysInfo
        anchors.centerIn: parent
        text: "Keys: Esc, Arrows, Z, X"
        color: "white"
        font.pointSize: 18
    }

    Timer {
        id: keysTimer
        interval: 3000
        onTriggered: keysInfo.visible = false
    }

    // ============== SOUNDS
    readonly property alias sndPlayerBullet: sndPlayerBullet
    SoundEffect {
        id: sndPlayerBullet
        source:"../../assets/sfx/player bullet.wav"
    }

    readonly property alias sndPlayerDropBomb: sndPlayerDropBomb
    SoundEffect {
        id: sndPlayerDropBomb
        source:"../../assets/sfx/player drop bomb.wav"
    }

    readonly property alias sndPlayerDeath: sndPlayerDeath
    SoundEffect {
        id: sndPlayerDeath
        source:"../../assets/sfx/player death.wav"
    }

    readonly property alias sndHit: sndHit
    SoundEffect {
        id: sndHit
        source:"../../assets/sfx/hit.wav"
    }

    readonly property alias sndEnemyBullet: sndEnemyBullet
    SoundEffect {
        id: sndEnemyBullet
        source:"../../assets/sfx/enemy bullet.wav"
    }

    readonly property alias sndEnemyDeath: sndEnemyDeath
    SoundEffect {
        id: sndEnemyDeath
        source:"../../assets/sfx/enemy death.wav"
    }

    readonly property alias sndExplosion: sndExplosion
    SoundEffect {
        id: sndExplosion
        source:"../../assets/sfx/explosion.wav"
    }
}
