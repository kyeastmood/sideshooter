import QtQuick 2.0
import Felgo 3.0

import "../common"

EntityBase {
    id: root
    entityType: "playerBomb"
    width: 20
    height: 10

    // Allows setting initial velocity
    readonly property alias collider: collider

    Rectangle {
        anchors.fill: collider
        radius: 4
        color: "white"
    }

    BoxCollider {
        id: collider
        anchors.centerIn: parent
        categories: Box.Category3 // Bullets and bombs in Category 3
        collidesWith: Box.Category4 | Box.Category5 // Collides with: 4 = bricks, 5 = enemy entity

        fixture.onBeginContact: {
            // Destroy entity on any collision
            // provided by the gameScene
            sndExplosion.play()
            root.removeEntity()
        }
    }

    // SmoothMotionController slowly decelerates on x axis, and accelerates on y axis (aka "gravity")
    // which gives bombs some "momentum"
    SmoothMotionController {
        collider: collider
        xTargetVelocity: 0
        xAbsAcceleration: 100
        yTargetVelocity: 1000
        yAbsAcceleration: 1000
    }

    // This timer is a security measure, in case the bomb doesn't collide with anything
    Timer {
        id: timer
        running: true
        interval: 5000
        onTriggered: root.removeEntity()
    }
}
