import QtQuick 2.0
import QtGraphicalEffects 1.12
import Felgo 3.0

import "../common"

// Component of player's aircraft
EntityBaseDraggable {
    id: root
    entityType: "player"
    width: 40
    height: 20

    readonly property alias energy: priv.energy

    // Setting colliderComponent is required for dragging in LevelEditor, however during
    // gameplay it must be switched off because the entity will collide with everything.
    colliderComponent: gameLogic && gameLogic.isInEditor ? collider : undefined
    selectionMouseArea.anchors.fill: root

    onEntityPressAndHold: removeEntity()

    // Keep the number of contacts so subsequent collisions do not reduce energy
    property int brickContacts: 0

    BoxCollider {
        id: collider
        anchors.centerIn: parent
        width: 20
        height: 10
        categories: Box.Category2 // Only player is in category 2
        // 4 = bricks; 5 = enemy units; 6 = enemy bullets, enemy radars;
        collidesWith: Box.Category4 | Box.Category5| Box.Category6

        fixture.onBeginContact: {
            var body = other.getBody()
            var collidedEntity = body.target
            var collidedEntityType = collidedEntity.entityType

            if (collidedEntityType === "brick") {
                ++brickContacts
                if (brickContacts == 1) {
                    hitTimer.entityId = collidedEntityType
                    hitTimer.running = true
                }
            }
            else if (collidedEntityType === "enemyBullet" || collidedEntityType === "enemyBulletStronger") {
                hitTimer.entityId = collidedEntityType
                hitTimer.running = true
            }
        }

        fixture.onEndContact: {
            var body = other.getBody()
            var collidedEntity = body.target
            var collidedEntityType = collidedEntity.entityType
            if (collidedEntityType === "brick") {
                --brickContacts
            }
        }
    }

    // Delays player's energy decrease so in effect e.g. simultaneous collisions are counted
    // as only one collision which is more fair to the player.
    Timer {
        id: hitTimer
        interval: 50
        property string entityId
        onTriggered: {
            if (entityId === "enemyBulletStronger")
                priv.energy -= 2
            else
                --priv.energy
            // provided by the gameScene
            if (energy == 0)
                sndPlayerDeath.play()
            else
                sndHit.play()
            mask.visible = true
            maskTimer.start()
            if (!priv.energy) {
                root.removeEntity()
            }
        }
    }

    // Keyboard input will be provided to controller from outside
    property alias controller: twoAxisController

    /*
     Aircraft motion is controlled on every axis separately, either by TwoAxisController
     or motionRandomizer.
     TwoAxisController proposes target velocities when player presses arrow keys,
     otherwise target velocity on the given axis is set to 0.
     When TwoAxisController sets target velocity to 0 because there is no user input on a given axis,
     the motionRandomizer starts providing random low target velocities at regular (but slightly random)
     intervals, so the aircraft floats.
    */
    TwoAxisController {
        id: twoAxisController

        // Target velocities are 0 if there are no pressed arrow keys for a given axis
        readonly property int xTargetVelocity: xAxis * priv.absMaxVelocity
        readonly property int yTargetVelocity: -yAxis * priv.absMaxVelocity

        inputActionsToKeyCode: {
            "up": Qt.Key_Up,
            "down": Qt.Key_Down,
            "left": Qt.Key_Left,
            "right": Qt.Key_Right,
            "shootBullet": Qt.Key_Z,
            "dropBomb": Qt.Key_X,
        }

        onInputActionPressed: {
            if(actionName == "left")
                priv.aircraftDirection = -1
            else if(actionName == "right")
                priv.aircraftDirection = 1
            else if(actionName == "dropBomb")
                dropBomb()
            else if(actionName == "shootBullet")
                shootBullet()
        }
    }

    // SmoothMotionController gradually increases/decreases velocity until it reaches target velocity,
    // which results in smooth motion.
    SmoothMotionController {
        id: smoothMotionController
        // SmoothMotionController updates linear velocities of collider
        collider: collider
        /* If TwoAxisController set correct target velocity (different than 0), we use it;
           otherwise we get some random low target velocity (close to 0) from motionRandomizer.
           We also change acceleration depending on the state of the aircraft - if it's close to stop
           we use lower acceleration which adds some "floatiness". */
        xTargetVelocity: twoAxisController.xTargetVelocity ? twoAxisController.xTargetVelocity
                                                           : motionRandomizer.xTargetVelocity
        xAbsAcceleration: xAlmostStopped ? 50 : 1000
        yTargetVelocity: twoAxisController.yTargetVelocity ? twoAxisController.yTargetVelocity
                                                           : motionRandomizer.yTargetVelocity
        yAbsAcceleration: yAlmostStopped ? 50 : 1000
        // Threshold below which low acceleration value is used.
        absLowVelocityThreshold: 50
    }

    /* Randomizes velocity on idle axes, which results in "floaty" aircraft behaviour.
       In case when player released all keys, two axes are randomized.
       In case when only one axis of the aircraft is controlled (e.g. flying left/right),
       only the other axis is randomized.
       These values are then read by SmoothMotionController.
       */
    Timer {
        id: motionRandomizer
        // Only run if there is at least one idle axis.
        running: !twoAxisController.xTargetVelocity || !twoAxisController.yTargetVelocity
        onRunningChanged: if (running) onTriggered()
        repeat: true

        property int xTargetVelocity
        property int yTargetVelocity
        readonly property int absMaxFloatingSpeed: 10

        /* Generates random target velocity on idle axes,
         in the range (-absMaxFloatingSpeed, absMaxFloatingSpeed).
         Also randomizes next interval.
         */
        onTriggered: {
            if (!twoAxisController.xTargetVelocity)
                xTargetVelocity = utils.generateRandomValueBetween(-absMaxFloatingSpeed,
                                                                   absMaxFloatingSpeed)

            if (!twoAxisController.xTargetVelocity)
                yTargetVelocity = utils.generateRandomValueBetween(-absMaxFloatingSpeed,
                                                                   absMaxFloatingSpeed)

            interval = utils.generateRandomValueBetween(1000, 1500)
        }
    }

    function shootBullet() {
        var properties = {
            "collider.linearVelocity.x": priv.aircraftDirection * 500,
            "collider.linearVelocity.y": utils.generateRandomValueBetween(-25, 25)
        }
        var entityId = entityManager.createEntityFromUrlWithProperties(Qt.resolvedUrl("PlayerBullet.qml"), properties)
        var entity = entityManager.getEntityById(entityId)
        priv.centerCreatedProjectile(entity)
        // provided by the gameScene
        sndPlayerBullet.play()
    }

    function dropBomb() {
        // Bomb's velocities are set to same as aircraft's which gives them some
        // initial "momentum"
        var properties = {
            "collider.linearVelocity.x": collider.linearVelocity.x
                                         + utils.generateRandomValueBetween(-25, 25),
            "collider.linearVelocity.y": collider.linearVelocity.y
        }
        var entityId = entityManager.createEntityFromUrlWithProperties(Qt.resolvedUrl("Bomb.qml"), properties)
        var entity = entityManager.getEntityById(entityId)
        priv.centerCreatedProjectile(entity)
        // provided by the gameScene
        sndPlayerDropBomb.play()
    }

    QtObject {
        id: priv

        // Max velocity for aircraft, absolute value.
        readonly property int absMaxVelocity: 300

        // -1 means left, 1 means right. Used for shooting and flipping the sprite
        property int aircraftDirection: 1

        property int energy: 10

        function centerCreatedProjectile(entity) {
            entity.x = root.x + (root.width - entity.width) / 2
            entity.y = root.y + (root.height - entity.height) / 2
        }
    }

    SpriteSequence {
        id: sprite
        anchors.centerIn: parent
        width: root.width
        height: root.height
        mirrorX: priv.aircraftDirection < 0
        Sprite {
            frameCount: 1
            frameRate: 10
            frameWidth: root.width * 2
            frameHeight: root.height * 2

            source: Qt.resolvedUrl("../../assets/player.png")
        }
    }

    // The mask covers entity with white color for a moment when hit
    Rectangle {
        id: maskRect
        color: "white"
        anchors.fill: root
        visible: false
    }

    OpacityMask {
        id: mask
        anchors.fill: sprite
        source: maskRect
        maskSource: sprite
        visible: false
        cached: false
    }

    Timer {
        id: maskTimer
        interval: 33
        onTriggered: mask.visible = false
    }

    // Notify game logic of existence of Player so it is easly accesible.
    // gameLogic is defined in GameScene
    Component.onCompleted: gameLogic.player = root
}
