import QtQuick 2.0
import Felgo 3.0

EntityBase {
    id: root
    entityType: "playerBullet"
    width: 10
    height: 10

    // Allows setting initial velocity
    readonly property alias collider: collider

    Rectangle {
        anchors.centerIn: parent
        width: root.width
        height: 3
        color: "white"
    }

    BoxCollider {
        id: collider
        anchors.centerIn: parent
        categories: Box.Category3 // Bullets and bombs in Category 3
        collidesWith: Box.Category4 | Box.Category5 // Collides with: 4 = bricks, 5 = enemy entity

        fixture.onBeginContact: {
            // Destroy entity on any collision
            root.removeEntity()
        }
    }

    // This timer is a security measure, in case the bullet doesn't collide with anything
    Timer {
        id: timer
        running: true
        interval: 1000
        onTriggered: root.removeEntity()
    }
}
