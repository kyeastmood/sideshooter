import QtQuick 2.0
import Felgo 3.0

EnemyAircraft {
    id: root
    entityType: "enemyStronger"
    energy: 4
    maxVelocity: 100
    bulletVelocity: 250
    width: 80
    height: 80
    spriteUrl: Qt.resolvedUrl("../../assets/enemy2.png")
    bulletFileName: Qt.resolvedUrl("EnemyBulletBig.qml")
}
