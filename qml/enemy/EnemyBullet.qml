import QtQuick 2.0
import Felgo 3.0

EntityBase {
    id: root
    entityType: "enemyBullet"
    width: 20
    height: 20

    readonly property alias collider: collider
    property string spriteUrl: Qt.resolvedUrl("../../assets/enemy1bullet.png")

    BoxCollider {
        id: collider
        anchors.centerIn: parent
        categories: Box.Category6 // 6 = enemy bullet and radar
        collidesWith: Box.Category2 | Box.Category4

        fixture.onBeginContact: {
            // Destroy entity on any collision
            root.removeEntity()
        }
    }

    Timer {
        id: timer
        running: true
        interval: 1000
        onTriggered: root.removeEntity()
    }

    SpriteSequence {
        id: sprite
        anchors.centerIn: parent
        width: root.width
        height: root.height

        Sprite {
            frameCount: 1
            frameRate: 10
            frameWidth: root.width * 2
            frameHeight: root.height * 2

            source: spriteUrl
        }
    }
}
