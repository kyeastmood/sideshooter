import QtQuick 2.0
import QtGraphicalEffects 1.12
import Felgo 3.0

import "../common"

/* EnemyAircraft can follow the player diagonally and shoot bullets diagonally. */
EntityBaseDraggable {
    id: root
    entityType: "enemyAircraft"
    width: 40
    height: 40
    // Setting colliderComponent is required for dragging in LevelEditor, however during
    // gameplay it must be switched off because the entity will collide with everything.
    colliderComponent: gameLogic && gameLogic.isInEditor ? collider : undefined
    selectionMouseArea.anchors.fill: root

    onEntityPressAndHold: removeEntity()

    property int energy: 2

    property int maxVelocity: 50
    property int bulletVelocity: 200

    property string spriteUrl: Qt.resolvedUrl("../../assets/enemy1.png")
    property string bulletFileName: Qt.resolvedUrl("EnemyBullet.qml")

    BoxCollider {
        id: collider
        anchors.centerIn: parent
        categories: Box.Category5 // 5 enemy units
        collidesWith: Box.Category3 | Box.Category4 // 3 = player bullet, 4 = brick

        // DirectionController computes the scale factor and direction of velocity for a given axis
        linearVelocity.x: directionController.xAxisFactor * maxVelocity
        linearVelocity.y: directionController.yAxisFactor * maxVelocity

        fixture.onBeginContact: {
            var body = other.getBody()
            var collidedEntity = body.target
            var collidedEntityType = collidedEntity.entityType

            if(collidedEntityType === "playerBullet" || collidedEntityType === "playerBomb") {
                --energy
                // provided by the gameScene
                if (energy == 0)
                    sndEnemyDeath.play()
                else
                    sndHit.play()
                mask.visible = true
                maskTimer.start()
                if (!energy) {
                    gameLogic.onEnemyDestroyed(root.entityType)
                    root.removeEntity()
                }
            }
            // Stops directionController so patrolTimer will choose another location
            else if (collidedEntityType === "brick") {
                directionController.targetPoint = Qt.point(-1,-1)
            }
            // Physics manager overwrites collider velocity during collision so we have
            // to restore the bindings.
            collider.linearVelocity.x = Qt.binding(function () {return directionController.xAxisFactor * maxVelocity})
            collider.linearVelocity.y = Qt.binding(function () {return directionController.yAxisFactor * maxVelocity})
        }
    }

    /* This collider represents a radar. On collision with the player, player's entity
       is set as target for DirectionController, which sets velocity factor for each axis. */
    BoxCollider {
        id: radarCollider
        height: 300
        width: 300
        collisionTestingOnlyMode: true
        anchors.centerIn: root
        categories: Box.Category6 // 6 = enemy bullet and radar
        collidesWith: Box.Category2 // 2 = player
        visible: false
        Rectangle {
            color: "orange"
            opacity: 0.3
            anchors.fill: parent
        }

        fixture.onBeginContact: {
            var body = other.getBody()
            var collidedEntity = body.target
            var collidedEntityType = collidedEntity.entityType

            if (collidedEntityType === "player")
                directionController.targetEntity = collidedEntity
        }

        fixture.onEndContact: {
            var body = other.getBody()
            var collidedEntity = body.target
            var collidedEntityType = collidedEntity.entityType

            // Clearing targetEntity will stop the aircraft
            if (collidedEntityType === "player")
                directionController.targetEntity = null
        }
    }

    // Moves the aircraft towards the target in a manner that target is reached on both
    // axes simultaneously and maximum velocity is never exceeded diagonally.
    DirectionController {
        id: directionController
        observerEntity: root
    }

    // Chooses random location to reach when player is not in range
    Timer {
        id: patrolTimer
        running: !directionController.targetEntity
        repeat: true
        interval: 1000
        onTriggered: {
            var range = 100
            var x = utils.generateRandomValueBetween(root.x - range,
                                                     root.x + root.width + range)
            var y = utils.generateRandomValueBetween(root.y - range,
                                                     root.y + root.height + range)
            directionController.targetPoint = Qt.point(x,y)
        }
    }

    Timer {
        id: shootTimer
        running: directionController.targetEntity
        interval: 1000
        repeat: true
        onTriggered: {
            var properties = {
                "collider.linearVelocity.x": directionController.xAxisFactor * bulletVelocity,
                "collider.linearVelocity.y": directionController.yAxisFactor * bulletVelocity
            }
            var entityId = entityManager.createEntityFromUrlWithProperties(Qt.resolvedUrl(bulletFileName), properties)
            var entity = entityManager.getEntityById(entityId)
            priv.centerCreatedProjectile(entity)
            // provided by the gameScene
            sndEnemyBullet.play()
        }
    }

    QtObject {
        id: priv

        function centerCreatedProjectile(entity) {
            entity.x = root.x + (root.width - entity.width) / 2
            entity.y = root.y + (root.height - entity.height) / 2
        }
    }

    SpriteSequence {
        id: sprite
        anchors.fill: root
        width: root.width
        height: root.height
        Sprite {
            frameCount: 1
            frameRate: 10
            frameWidth: root.width * 2
            frameHeight: root.height * 2
            source: root.spriteUrl
        }
    }

    // The mask covers entity with white color for a moment when hit
    Rectangle {
        id: maskRect
        color: "white"
        anchors.fill: root
        visible: false
    }

    OpacityMask {
        id: mask
        anchors.fill: sprite
        source: maskRect
        maskSource: sprite
        visible: false
    }

    Timer {
        id: maskTimer
        interval: 33
        onTriggered: mask.visible = false
    }

    Component.onCompleted: {
        // This check is a quick workaround for a problem that some component
        // creates enemies at 0,0, maybe LevelEditor.
        if (x > 0 && y > 0)
            ++gameLogic.createdEnemyCount
    }
}
