import QtQuick 2.0
import Felgo 3.0

EnemyBullet {
    id: root
    width: 40
    height: 40
    entityType: "enemyBulletStronger"
    spriteUrl: Qt.resolvedUrl("../../assets/enemy2bullet.png")
}
