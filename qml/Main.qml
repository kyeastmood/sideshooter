import Felgo 3.0
import QtQuick 2.0


import "scenes"

GameWindow {
    id: gameWindow
    shutdownAppWithEscapeKeyEnabled: false

    // the size of the Window can be changed at runtime by pressing Ctrl (or Cmd on Mac) + the number keys 1-8
    // the content of the logical scene size (480x320 for landscape mode by default) gets scaled to the window size based on the scaleMode
    // you can set this size to any resolution you would like your project to start with, most of the times the one of your main target device
    // this resolution is for iPhone 4 & iPhone 4S
    screenWidth: 960
    screenHeight: 640

    MenuScene {
        id: menuScene
        onStartGamePressed: {
            gameWindow.state = "game"
            gameScene.startGame()
        }
        onStartEditorPressed:  {
            gameWindow.state = "editor"
            editorScene.startGame()
        }
        onExitPressed: Qt.quit()
    }

    GameScene {
        id: gameScene
        onEscapePressed: {
            gameWindow.state = "menu"
            gameScene.closeGame()
        }
        onGameFinished: {
            gameOverScene.victory = victory
            gameOverScene.score = score
            gameWindow.state = "over"
            gameScene.closeGame()
        }
    }

    EditorScene {
        id: editorScene
        onEscapePressed: {
            gameWindow.state = "menu"
            editorScene.closeGame()
        }
    }

    GameOverScene {
        id: gameOverScene
        onMenuRequested: {
            gameWindow.state = "menu"
        }
    }

    state: "menu"

    states: [
        State {
            name: "menu"
            PropertyChanges {target: menuScene; visible: true}
            PropertyChanges {target: gameWindow; activeScene: menuScene}
        },
        State {
            name: "editor"
            PropertyChanges {target: editorScene; visible: true}
            PropertyChanges {target: gameWindow; activeScene: editorScene}
        },
        State {
            name: "game"
            PropertyChanges {target: gameScene; visible: true}
            PropertyChanges {target: gameWindow; activeScene: gameScene}
        },
        State {
            name: "over"
            PropertyChanges {target: gameOverScene; visible: true}
            PropertyChanges {target: gameWindow; activeScene: gameOverScene}
        }
    ]
}
